package INF101.lab2;

import java.util.NoSuchElementException;


import java.util.ArrayList;



public class Fridge implements IFridge{

    ArrayList<FridgeItem> fridgeItems = new ArrayList<FridgeItem>();
    
    public void emptyFridge(){      //Sier at kjøleskapet er tomt
        fridgeItems.clear();
    }

    public int totalSize() {        //Sier at totalSize er lik 20, maks antall gjennstander er lik 20
        return 20;
    }

    public int nItemsInFridge() {   //size() method of the List interface in Java is used to get the number of elements in this list
        return fridgeItems.size();
    }

    public boolean placeIn(FridgeItem item){    //Lager en boolean for itemet som skal inn i kjøleskapet
        if(fridgeItems.size() < totalSize()) {  // Så lenge fridgeItems er mindre enn totalSize så legger de til item
            fridgeItems.add(item);
            return true;
        }
        else{ 
            return false;
        }
    }

    public ArrayList<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expired = new ArrayList<FridgeItem>();
    
        for (FridgeItem food : fridgeItems) {
            if (food.hasExpired()){
                expired.add(food);
            }
        }
        for (FridgeItem food : expired) {
            takeOut(food);
        }     
    return expired;
    }



	public void takeOut(FridgeItem item){
        if(fridgeItems.contains(item)){
        fridgeItems.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }
        	/**
	 * Remove item from fridge
	 * 
	 * @param item to be removed
	 * @throws NoSuchElementException if fridge does not contain <code>item</code>
	 */


    }
    
    

